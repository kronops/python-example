# python-example

## Introduction

You can use this repo as an example to build your own docker image with
Python with Flask.

### Objetives

Demonstrate how to build a CI process with Docker and GitLab CI.

## Requirements

You are going to need a machine with docker, only if you want to
build locally your images. You are going to add your user to docker
group before you can run docker on your machine.

## Build image

Let's move to the location:

```shell
$ cd ~/data/vc/kronops/python-examaple
```

Run docker build and set your image tag:

```shell
$ docker build -t kronoops/python-example:1.0 .
```

This step finishes with something like this:

```shell
Removing intermediate container dc7ce4398370
 ---> a7c2d14b18f2
Step 5/6 : ENTRYPOINT ["python"]
 ---> Running in af993279342c
Removing intermediate container af993279342c
 ---> 288c661b92a1
Step 6/6 : CMD ["helloworld.py"]
 ---> Running in 964e709e03f2
Removing intermediate container 964e709e03f2
 ---> 252317629c62
Successfully built 252317629c62
Successfully tagged poder/python-example:1.0
```
