# Build image based on alpine python image:
# https://hub.docker.com/_/python
FROM python:alpine3.7

# Copy local files to app directory
COPY . /app

# Change to app directory
WORKDIR /app

# Install modules with pip
RUN pip install -r requirements.txt

# Use python as entrypoint
ENTRYPOINT ["python"]

# and run this script
CMD ["helloworld.py"]
